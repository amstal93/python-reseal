.. include:: substitutions

ReSeal
######

.. note::

  |api-build-note|


The core ReSeal library consists primarily of two components, the cache (ReCache), and the Microsoft Simple Encrypted Arithmetic Library (SEAL) abstraction (ReSeal).

.. autoclass:: fhe.reseal.Reseal
  :members:

.. autoclass:: fhe.reseal.ReSeal
  :members:

.. autoclass:: fhe.recache.ReCache
  :members:

.. autoclass:: fhe.rescheme.ReScheme
  :members:
