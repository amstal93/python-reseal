.. include:: substitutions

Node
====

.. autoclass:: fhe.nn.graph.node.Node
  :members:
