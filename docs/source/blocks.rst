.. include:: substitutions

.. _section_blocks:

Blocks
===============

.. note::

  |api-build-note|

There are several main types of neural network components we implement.

Blocks
++++++

.. autoclass:: fhe.nn.block.block.Block
  :members:
