.. include:: substitutions

.. _section_layers:

Layers
===============

.. note::

  |api-build-note|

Layers
++++++

.. autoclass:: fhe.nn.layer.layer.Layer
  :members:
