.. include:: substitutions

.. _section_activations:

Activations
===============

.. note::

  |api-build-note|

Activations
+++++++++++

.. autoclass:: fhe.nn.activation.activation.Activation
  :members:
