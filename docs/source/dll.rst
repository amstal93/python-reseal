.. include:: substitutions

Doubly Linked List
==================

.. autoclass:: fhe.nn.graph.dll.DLL
  :members:
