sphinx>=3.5.1
sphinx-argparse>=0.2.5
sphinx-rtd-theme>=0.5.2
ConfigArgParse>=0.14.0
future>=0.17.1
networkx>=2.5.1
python-igraph>=0.9.1
